// script.js
document.addEventListener('DOMContentLoaded', function () {
    const searchButton = document.getElementById('searchButton');
    const titleInput = document.getElementById('t');
    const movieInfo = document.getElementById('movieInfo');
  
    searchButton.addEventListener('click', function () {
      const title = titleInput.value.trim(); // Elimina espacios al inicio y al final
  
      fetch(`https://www.omdbapi.com/?t=${title}&plot=full&apikey=30063268`)
        .then(response => response.json())
        .then(data => {
          // Manipular la data y mostrarla en la página
          if (data.Title.toLowerCase() === title.toLowerCase()) {
            const movieDetails = `
              <h2>${data.Title} (${data.Year})</h2>
              <div id="actorsInfo">
                <p><strong>Actores:</strong> ${data.Actors}</p>
              </div>
              <div id="plotInfo">
                <p><strong>Reseña:</strong> ${data.Plot}</p>
              </div>
              <img src="${data.Poster}" alt="${data.Title} Poster">
            `;
            movieInfo.innerHTML = movieDetails;
          } else {
            // Limpiar la información si el título no coincide exactamente
            movieInfo.innerHTML = '<p>No se encontró una película con ese título.</p>';
          }
        })
        .catch(error => console.error(error));
    });
});
